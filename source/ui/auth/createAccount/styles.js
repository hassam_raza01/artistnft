import { StyleSheet, Dimensions } from 'react-native';
import theme from 'source/constants/colors';

export default StyleSheet.create({
    container: {
        alignItems: 'center',
        backgroundColor: theme.primary,
        minHeight: Dimensions.get('window').height,
    },
    appName: {
        height: 22,
        marginTop: 35,
        resizeMode: 'contain',
    },
    heading1: {
        fontSize: 18,
        marginTop: 50,
        color: 'white',
        textAlign: 'center',
        fontFamily: 'Sansation_Bold'
    },
    heading2: {
        fontSize: 16,
        marginTop: 22,
        color: '#C6C6C6',
        marginBottom: 20,
        textAlign: 'center',
        fontFamily: 'Sansation_Bold'
    },
    heading3: {
        fontSize: 14,
        color: '#FFF',
        marginLeft: 10,
        fontFamily: 'Sansation_Bold'
    },
    checkRow: {
        width: '100%',
        paddingLeft: 15,
        marginTop: 20,
        alignItems: 'center',
        flexDirection: 'row',
    }
})