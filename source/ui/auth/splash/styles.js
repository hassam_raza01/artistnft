import { StyleSheet } from 'react-native';
import theme from 'source/constants/colors';

export default StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: theme.primary
    },
    logo: {
        width: 130,
        height: 130,
    },
    appName: {
        height: 24,
        marginTop: 18,
        resizeMode: 'contain',
    }
})