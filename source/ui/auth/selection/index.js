import React, { useEffect, useState } from 'react';
import { View, Image, Text, ScrollView, TouchableWithoutFeedback } from 'react-native';
import styles from './styles';

import IMAGE from 'source/assets/splash1.png';
import APP_NAME from 'source/assets/app_name.png';
import Dots from './components/Dots';
import Button from 'source/common/button';
import OutlinedButton from 'source/common/button/OutlinedButton';
import LanguageSelector from 'source/common/languageSelector';
import { getRoles } from 'source/redux/actions/utils';
import { useDispatch } from 'react-redux';

export default function Selection({ navigation }) {
    const dispatch = useDispatch()
    const [toggler, setToggler] = useState(false)

    useEffect(() => {
        dispatch(getRoles())
    }, [])

    return (
        <ScrollView contentContainerStyle={styles.scrollViewContainer}>
            <TouchableWithoutFeedback onPress={() => setToggler(Math.random())}>
                <View style={styles.container}>
                    <LanguageSelector toggler={toggler} />
                    
                    <Image
                        source={APP_NAME}
                        style={styles.appName}
                    />

                    <Image
                        source={IMAGE}
                        style={styles.image}
                    />

                    <Text style={styles.heading1}>{'PLEASE CREATE AN\nACCOUNT OR SIGNIN'}</Text>

                    <Text style={styles.heading2}>{'Please choose if you would like to\nsignin or sigup as a Artist or a Fan'}</Text>

                    <Dots active={0} />

                    <View style={{ marginTop: 'auto', width: '100%', paddingBottom: 15 }}>
                        <Button
                            style={{ marginTop: 50, marginLeft: 'auto', marginRight: 'auto' }}
                            title="CREATE ACCOUNT"
                            onPress={()=>navigation.navigate('CreateAs')}
                        />
                        
                        <OutlinedButton
                            title="SIGN IN"
                            style={{ marginTop: 15, marginLeft: 'auto', marginRight: 'auto' }}
                            onPress={()=>navigation.navigate('Login')}
                        />
                    </View>
                </View>
            </TouchableWithoutFeedback>    
        </ScrollView>
    )
}