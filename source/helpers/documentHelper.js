import DocumentPicker from 'react-native-document-picker';

export const pickFile = async () => {
  // Pick a single file
  try {
    const res = await DocumentPicker.pick({
      type: [DocumentPicker.types.allFiles],
    });
    let first = res[0];
    console.log(first);
    console.log(
      first.uri,
      first.type, // mime type
      first.name,
      first.size,
    );
  } catch (err) {
    if (DocumentPicker.isCancel(err)) {
      // User cancelled the picker, exit any dialogs or menus and move on
    } else {
      throw err;
    }
  }
};
